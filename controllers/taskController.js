/*
	https://mongoosejs.com/docs/queries.html

		Mongoose Queries

			Model.deleteMany()
			Model.deleteOne()
			Model.find()
			Model.findById()
			Model.findByIdAndDelete()
			Model.findByIdAndRemove()
			Model.findByIdAndUpdate()
			Model.findOne()
			Model.findOneAndDelete()
			Model.findOneAndRemove()
			Model.findOneAndReplace()
			Model.findOneAndUpdate()
			Model.replaceOne()
			Model.updateMany()
			Model.updateOne()

*/




// It will allow us to use the contents of the task.js file in the model folder.

const Task = require("../models/task");


module.exports.getAllTasks = () =>{
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	return Task.find({}).then(result => result)
} 

// Create a Task
// "createTask" is from routes/taskRoute.js
module.exports.createTask = (reqBody) => {

	// Create a task object based on the mongoose model "Task"
	// Instantiate
	let newTask = new Task({
		// Sets the "name" property with the value received from postman
		name: reqBody.name
	})

	// Using callback function: newUser.save((saveErr, saveTask => { })
	// Using .then method: newUser.save().then((task, error) => { })
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error)
			return false
		}
		else {
			// Returns the new task object saved in the database back to the postman
			return task;
		}
	})
}


// Delete a Task
// Business Logic
/*
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removeTask, err) =>{
		if(err){
			console.log(err);
			return false;
		}
		else{
			return removeTask;
		}

	})
}

// Update a Task
// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. Save the task
*/

module.exports.updateTask = (taskId, reqBody) =>{
	// findById is the same as "find({"_id":"value"})"
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error)
			return false;
		}
		else{

			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) =>{
				if (updateErr) {
					console.log(updateErr);
					return false;
				}
				else {
					return updatedTaskName;
				}
			})
		}
	})
}

module.exports.getOneTasks = (taskId) =>{
	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	return Task.findById(taskId).then((getOneTask, getOneErr) =>{
		if(getOneErr){
			console.log(getOneErr);
			return false;
		}
		else{
			return getOneTask;
		}

	})
} 




module.exports.completeTask = (taskId) =>{
	// findById is the same as "find({"_id":"value"})"
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error)
			return false;
		}
		else{
			
			result.status = "complete";

			return result.save().then((completeTaskStatus, completeErr) =>{
				if (completeErr) {
					console.log(completeErr);
					return false;
				}
				else {
					return completeTaskStatus;
				}
			})
		}
	})
}