/*
	Setup the dependencies
		1. Express -> npm install express
		2. Mongoose -> npm install mongoose
*/
const express = require("express");
const mongoose = require("mongoose");

// This allow us to use all the routes defined in the "TaskRoute.js"
const taskRoute = require("./routes/taskRoute")


// Server Setup
const app = express();
const port = 3001;

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://system:admin@myhelloword.t5o0pyw.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true});

// Set a notification for connection success or error with our database.
let db = mongoose.connection;

// Notify on error
db.on("error", console.error.bind(console, "Connection Error"));

// if the connection is successfull, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database Session 31"));

// Add task route
// Allow all the task routes created in the "taskRoute.js" file to use "tasks" route
app.use("/task", taskRoute);



// Serrver Listening to the port
app.listen(port, () => console.log(`Server running at port ${port}`));


/*
	Separation of Concerns

		Model Folders
		- Contains the Object and Schemas and defines the object structure and content

		Controllers Folder
		- Contain the function and business logic of our JS application.
		- Meaning all the operations it can do will be placed here.

		Routes Folder
		- Contain all the endpoint, and assign the http methods for our application.
		- We separate the routes such that the server/"index" only contains information on the server


		JS Modules 
			require => to include a specific module/package
			export.module => to treat a value as a "package" that can be used by other files.
		
		Flow of exports and requires: 

		export models > require in controllers
		export controllers > require in routes
		export routes > require in server (index.js)



*/