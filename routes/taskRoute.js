
const express = require("express");

// The "taskController" allow us to use the function defined inside it
const taskController = require("../controllers/taskController")

// Allows access to Http method middlewares that makes it easier to create routes for our applcaition
const router = express.Router()

// Routes to get all tasks
router.get("/", (req, res) =>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

// Route to create a Task 
// localhost:3001/task it is the same with localhost:3001/task/
router.post("/", (req, res) =>{

	// The "createTask" function need data from the request body, so we need it to supply in the taskController.createTask(argument) 
	
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})



// Route to delete a task
// colon(:id) is an identifier thet helps create a dynamic route which allows us to supply information in the url
// ":id" is a wildcard where you can put the objectID as the value
// Ex: localhost:300/task/:id or localhost:3000/task/123456
router.delete("/:id", (req,res) => {
	// If information will be coming from the URL, the data can be accessed from the request "params" property
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

}) 

// Route to update a task using "patch" method 
router.patch("/:id", (req,res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


/*
	Instructions s31 Activity:
	1. Create a route for getting a specific task.
	2. Create a controller function for retrieving a specific task.
	3. Return the result back to the client/Postman.
	4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
	5. Create a route for changing the status of a task to "complete".
	6. Create a controller function for changing the status of a task to "complete".
	7. Return the result back to the client/Postman.
	8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
	9. Create a git repository named S31.
	10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	11. Add the link in Boodle.

*/


router.get("/:id", (req,res) =>{
	taskController.getOneTasks(req.params.id).then(resultFromController => res.send(resultFromController));
})

router.put("/:id/complete", (req,res) =>{
	taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
})







// Use "module.exports" to export the router object to be use in the server
module.exports = router;